package kr.ac.yonsei.crawler;
 
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
 
public class FetchOnePage {
	private static final String PERSON_LIST_URL = "http://movie.naver.com/movie/sdb/browsing/bpeople.nhn?nation=KR&page=";
	private static final String MOVIE_LIST_URL = "http://movie.naver.com/movie/bi/pi/filmoMission.nhn?peopleCode=%s&page=%s";
 
	public static void main(String[] args) {
//		fetchPersonList();
		
		// 1794, 박신양
		fetchMovieList("1794");
	}
 
	private static void fetchMovieList(String personId) {
		int page = 1;
		boolean hasNextPage = true;
		
		while (hasNextPage) {
			try {
				URL url = new URL(String.format(MOVIE_LIST_URL, personId, Integer.toString(page)));
				hasNextPage = fetchMovieList(url);
				page += 1;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
 
	private static boolean fetchMovieList(URL url) throws IOException {
		String line = null;
		boolean hasNext = false;
		
		try (BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
			while ((line = br.readLine()) != null) {
				if (line.contains("	<a href=\"/movie/bi/mi/basic.nhn?code=")) {
					String id = getId(line);
					String name = getName(line);
					System.out.println(id + "," + name);
				}
				
				if (line.contains("<em>다음</em>")) {
					hasNext = true;
				}
			}
		}
		
		return hasNext;
	}
 
	private static void fetchPersonList() {
		for (int page = 1; page <= 90; page++) {
			try {
				URL url = new URL(PERSON_LIST_URL + page);
				fetchPersonList(url);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
 
	private static void fetchPersonList(URL url) throws IOException {
		String line = null;
		try (BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream(), "EUC-KR"))) {
			while ((line = br.readLine()) != null) {
				if (line.contains("<a href=\"/movie/bi/pi/basic.nhn?code=")) {
					String id = getId(line);
					String name = getName(line);
					System.out.println(id + "," + name);
				}
			}
		}
	}
 
	private static String getId(String line) {
		int startIndex = line.indexOf("code=");
		int endIndex = line.indexOf("\"", startIndex+1);
		return line.substring(startIndex+5, endIndex);
	}
 
	private static String getName(String line) {
		int startIndex = line.indexOf("\">");
		int endIndex = line.indexOf("<", startIndex+1);
		return line.substring(startIndex+2, endIndex);
	}
}