package kr.ac.yonsei.crawler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class KevinBacon1 {
	private static final String PERSON_LIST_URL = "http://movie.naver.com/movie/sdb/browsing/bpeople.nhn?nation=KR&page=";
	private static final String MOVIE_LIST_URL = "http://movie.naver.com/movie/bi/pi/filmoMission.nhn?peopleCode=%s&page=%s";

	public static void main(String[] args) {
		HashMap<String, String> personIdToNameMap = new HashMap<>();
		
		fetchPersonList(personIdToNameMap, System.out);
		Set<Entry<String, String>> personEntrySet = personIdToNameMap.entrySet();

		fetchMovieList(personEntrySet, System.out);
	}
	
	private static void fetchMovieList(Set<Entry<String, String>> personEntrySet, PrintStream out) {
		for (Entry<String, String> personEntry : personEntrySet) {
			fetchMovieList(personEntry, out);
		}
	}

	private static void fetchMovieList(Entry<String, String> personEntry, PrintStream out) {
		int page = 1;
		boolean hasNextPage = true;
		
		while (hasNextPage) {
			try {
				URL url = new URL(String.format(MOVIE_LIST_URL, personEntry.getKey(), Integer.toString(page)));
				hasNextPage = fetchMovieList(url, personEntry, out);
				page += 1;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static boolean fetchMovieList(URL url, Entry<String, String> personEntry, PrintStream out) throws IOException {
		String line = null;
		boolean hasNext = false;
		
		try (BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
			while ((line = br.readLine()) != null) {
				if (line.contains("	<a href=\"/movie/bi/mi/basic.nhn?code=")) {
					String id = getId(line);
					String name = getName(line);
					out.println(personEntry.getKey() + "," + personEntry.getValue() + "," + id + "," + name);
				}
				
				if (line.contains("<em>다음</em>")) {
					hasNext = true;
				}
			}
		}
		
		return hasNext;
	}

	private static void fetchPersonList(HashMap<String, String> personIdToNameMap, PrintStream out) {
		for (int page = 1; page <= 90; page++) {
			try {
				URL url = new URL(PERSON_LIST_URL + page);
				fetchPersonList(url, personIdToNameMap, out);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static void fetchPersonList(URL url, HashMap<String, String> personIdToNameMap, PrintStream out) throws IOException {
		String line = null;
		try (BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream(), "EUC-KR"))) {
			while ((line = br.readLine()) != null) {
				if (line.contains("<a href=\"/movie/bi/pi/basic.nhn?code=")) {
					String id = getId(line);
					String name = getName(line);
					personIdToNameMap.put(id,  name);
					out.println(id + "," + name);
				}
			}
		}
	}

	private static String getId(String line) {
		int startIndex = line.indexOf("code=");
		int endIndex = line.indexOf("\"", startIndex+1);
		return line.substring(startIndex+5, endIndex);
	}

	private static String getName(String line) {
		int startIndex = line.indexOf("\">");
		int endIndex = line.indexOf("<", startIndex+1);
		return line.substring(startIndex+2, endIndex);
	}
}
