package kr.ac.yonsei.crawler;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

public class KevinBacon3 {
	private static final String PERSON_LIST_URL = "http://movie.naver.com/movie/sdb/browsing/bpeople.nhn?nation=KR&page=";
	private static final String MOVIE_LIST_URL = "http://movie.naver.com/movie/bi/pi/filmoMission.nhn?peopleCode=%s&page=%s";

	public static void main(String[] args) {
		try (PrintStream nodeSetOut = new PrintStream(new FileOutputStream("data\\movie_person_nodeset.txt"), true, "UTF-8");
			 PrintStream networkOut = new PrintStream(new FileOutputStream("data\\movie_person_network.txt"), true, "UTF-8")) {
			HashMap<String, String> personIdToNameMap = new HashMap<>();
			HashMap<String, List<String>> personToMovieMap = new HashMap<>();
			HashMap<String, List<String>> movieToPersonMap = new HashMap<>();

			try (Scanner inNodeset = new Scanner(new FileInputStream("data\\movie_nodeset.txt")); Scanner inNetwork = new Scanner(new FileInputStream("data\\movie_person_list.txt"))) {
//					fetchPersonList(personIdToNameMap);
					readPersonList(personIdToNameMap, inNodeset);
					
//					fetchMovieList(personIdToNameMap, personToMovieMap, movieToPersonMap);
					readMovieList(personToMovieMap, movieToPersonMap, inNetwork);
					
					writeNodeset(personIdToNameMap, nodeSetOut);
					writeOneModeNetwork(personIdToNameMap, personToMovieMap, movieToPersonMap, networkOut);
			}
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	private static void readPersonList(HashMap<String, String> personIdToNameMap, Scanner in) {
		while (in.hasNextLine()) {
			String line = in.nextLine();
			String[] cols = line.split(",");
			
			if (cols.length == 2) {
				personIdToNameMap.put(cols[0], cols[1]);
			}
		}
	}
	
	private static void readMovieList(HashMap<String, List<String>> personToMovieMap, HashMap<String, List<String>> movieToPersonMap, Scanner in) {
		String line = null;
		while (in.hasNextLine()) {
			try {
				line = in.nextLine();
				String[] cols = line.split(",", 3);
				add(personToMovieMap, cols[0], cols[2]);
				add(movieToPersonMap, cols[2], cols[0]);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private static void writeNodeset(HashMap<String, String> personIdToNameMap, PrintStream out) {
		for (Entry<String, String> entry : personIdToNameMap.entrySet()) {
			out.println(entry.getKey() + "," + entry.getValue());
		}
	}

	private static void writeOneModeNetwork(HashMap<String, String> personIdToNameMap, HashMap<String, List<String>> personToMovieMap, HashMap<String, List<String>> movieToPersonMap, PrintStream out) {
		for (Entry<String, String> personEntry : personIdToNameMap.entrySet()) {
			List<String> movies = personToMovieMap.get(personEntry.getKey());

			if (movies == null) {
				continue;
			}

			for (String movie : movies) {
				List<String> neighbors = movieToPersonMap.get(movie);

				for (String neighbor : neighbors) {
					if (personEntry.getKey().equals(neighbor)) {
						continue;
					}

					String edgeName = null;
					
					if (personEntry.getKey().compareTo(neighbor) > 0) {
						edgeName = personEntry.getKey() + ","	+ neighbor;
					}
					else {
						edgeName = neighbor + "," + personEntry.getKey();
					}

					out.println(edgeName);
				}
			}
		}
	}

	private static void fetchMovieList(HashMap<String, String> personIdToNameMap,
			HashMap<String, List<String>> personToMovieMap, HashMap<String, List<String>> movieToPersonMap) {
		Set<Entry<String, String>> personEntrySet = personIdToNameMap.entrySet();
		
		Counter counter = new Counter(personEntrySet.size());
		for (Entry<String, String> personEntry : personIdToNameMap.entrySet()) {
			counter.count("Fetch movie list of: " + personEntry.getValue());
			fetchMovieList(personEntry, personToMovieMap, movieToPersonMap);
		}
	}

	private static void fetchMovieList(Entry<String, String> personEntry,
			HashMap<String, List<String>> personToMovieMap, HashMap<String, List<String>> movieToPersonMap) {
		int page = 1;
		boolean hasNextPage = true;

		while (hasNextPage) {
			try {
				URL url = new URL(String.format(MOVIE_LIST_URL, personEntry.getKey(), Integer.toString(page)));
				hasNextPage = fetchMovieList(url, personEntry, personToMovieMap, movieToPersonMap);
				page += 1;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static boolean fetchMovieList(URL url, Entry<String, String> personEntry,
			HashMap<String, List<String>> personToMovieMap, HashMap<String, List<String>> movieToPersonMap)
			throws IOException {
		String line = null;
		boolean hasNext = false;

		try (BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
			while ((line = br.readLine()) != null) {
				if (line.contains("	<a href=\"/movie/bi/mi/basic.nhn?code=")) {
					String id = getId(line);

					add(personToMovieMap, personEntry.getKey(), id);
					add(movieToPersonMap, id, personEntry.getKey());
				}

				if (line.contains("<em>다음</em>")) {
					hasNext = true;
				}
			}
		}

		return hasNext;
	}

	private static void fetchPersonList(HashMap<String, String> personIdToNameMap) {
		// int total_pages = 90;
		int total_pages = 5;
		Counter counter = new Counter(total_pages);
		for (int page = 1; page <= total_pages; page++) {
			try {
				counter.count("Fetch person list: " + page);
				URL url = new URL(PERSON_LIST_URL + page);
				fetchPersonList(url, personIdToNameMap);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static void fetchPersonList(URL url, HashMap<String, String> personIdToNameMap) throws IOException {
		String line = null;
		try (BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream(), "EUC-KR"))) {
			while ((line = br.readLine()) != null) {
				if (line.contains("<a href=\"/movie/bi/pi/basic.nhn?code=")) {
					String id = getId(line);
					String name = getName(line);
					personIdToNameMap.put(id, name);
				}
			}
		}
	}

	private static void add(HashMap<String, List<String>> map, String key, String value) {
		if (map.containsKey(key) == false) {
			map.put(key, new ArrayList<String>());
		}

		map.get(key).add(value);
	}

	private static String getId(String line) {
		int startIndex = line.indexOf("code=");
		int endIndex = line.indexOf("\"", startIndex + 1);
		return line.substring(startIndex + 5, endIndex);
	}

	private static String getName(String line) {
		int startIndex = line.indexOf("\">");
		int endIndex = line.indexOf("<", startIndex + 1);
		return line.substring(startIndex + 2, endIndex);
	}

	static class Counter {
		public Counter(int total) {
			this.total = total;
			pauseInterval = 10;
			count = 0;
			fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		}

		public void count(String message) {
			count += 1;
			System.out.println(String.format("[%s] %4d / %4d - %s", fmt.format(new Date()), count, total, message));
			if (count % pauseInterval == 0) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

		private int total;
		private int count;
		private int pauseInterval;
		private DateFormat fmt;
	}
}
