package kr.ac.yonsei.crawler;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import org.graphstream.algorithm.ConnectedComponents;
import org.graphstream.algorithm.measure.ClosenessCentrality;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.DefaultGraph;

public class KevinBaconGame {
	public static void main(String[] args) throws FileNotFoundException {
		Graph graph = new DefaultGraph("KevinBacon", false, true);

		try (Scanner inNodeset = new Scanner(new FileInputStream("data\\movie_person_nodeset.txt"));
				Scanner inNetwork = new Scanner(new FileInputStream("data\\movie_person_network.txt"))) {
			buildGraph(graph, inNodeset, inNetwork);
			printGraphStatus(graph);

			List<Node> gccNodes = getGccNodes(graph);
			removeIsolated(graph, gccNodes);
			printGraphStatus(graph);
			centrality(graph);
			
			drawGraph(graph);
		}
	}

	private static void printGraphStatus(Graph graph) {
		ConnectedComponents c = new ConnectedComponents();
		c.init(graph);
		c.compute();

		System.out.println("Nodes: " + graph.getNodeCount() + " / Components: " + c.getConnectedComponentsCount());
	}

	private static void removeIsolated(Graph graph, List<Node> gccNodes) {
		Set<Node> removeNodes = new HashSet<>();
		
		for (Node node : graph) {
			if (gccNodes.contains(node) == false) {
				removeNodes.add(node);
			}
		}
		
		for (Node node : removeNodes) {
			graph.removeNode(node);
		}
	}

	private static List<Node> getGccNodes(Graph graph) {
		ConnectedComponents c = new ConnectedComponents();
		c.init(graph);
		c.compute();
		return c.getGiantComponent();
	}

	private static void centrality(Graph graph) {
		ClosenessCentrality c = new ClosenessCentrality("centrality");
		c.init(graph);
		c.compute();
		
		Node maxNode = null;
		double maxCentrality = 0.0;

		for (Node node : graph) {
			double centrality = node.getAttribute("centrality");
			if (maxCentrality < centrality) {
				maxNode = node;
				maxCentrality = centrality;
			}
		}
		
		// 유영현
		System.out.println(maxNode);
	}

	private static void buildGraph(Graph graph, Scanner inNodeset, Scanner inNetwork) {
		String line = null;

		HashMap<String, String> personIdToNameMap = new HashMap<>();

		while (inNodeset.hasNextLine()) {
			line = inNodeset.nextLine();
			String[] cols = line.split(",");

			if (cols.length == 2) {
				personIdToNameMap.put(cols[0], cols[1]);
			}
		}

		while (inNetwork.hasNextLine()) {
			line = inNetwork.nextLine();
			String[] cols = line.split(",");

			String edgeName = null;

			String node1 = personIdToNameMap.get(cols[0]);
			String node2 = personIdToNameMap.get(cols[1]);

			if (cols[0].compareTo(cols[1]) > 0) {
				edgeName = node1 + "," + node2;
			} else {
				edgeName = node2 + "," + node1;
			}


			if (node1 != null && node2 != null) {
				if (graph.getNode(node1) == null) {
					graph.addNode(node1);
				}
				
				if (graph.getNode(node2) == null) {
					graph.addNode(node2);
				}

				if (graph.getEdge(edgeName) == null) {
					graph.addEdge(edgeName, node1, node2);
				}
			}
		}
	}

	private static void drawGraph(Graph graph) {
		graph.addAttribute("ui.stylesheet", "node { size: 5px; }");
		graph.display();
	}
}
