package kr.ac.yonsei.datacleanser;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class DataCleanser {
	private InputStream is;

	public DataCleanser(InputStream is) {
		this.is = is;
	}

	public void run(OutputStream out) throws IOException {
		PrintWriter writer = new PrintWriter(out);

		Scanner scanner = new Scanner(is, "UTF-8");

		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			writer.println(line);
		}

		scanner.close();
		writer.flush();
	}
}
