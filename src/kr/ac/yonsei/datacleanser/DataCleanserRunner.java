package kr.ac.yonsei.datacleanser;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class DataCleanserRunner {
	public static void main(String[] args) {
//		runCleanser();
//		runCleanserWithRule2();
		runCleanserUsingJCSV();
	}

	private static void runCleanser() {
		try {
			DataCleanser cleanser = new DataCleanser(new FileInputStream("sample_data.csv"));
			cleanser.run(System.out);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void runCleanserWithRule2() {
		try {
			DataCleanserWithRule2 cleanser = new DataCleanserWithRule2(new FileInputStream("sample_data.csv"), ",");
			cleanser.run(new FileOutputStream("output.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void runCleanserUsingJCSV() {
		try {
			DataCleanserUsingJCSV cleanser = new DataCleanserUsingJCSV(new FileInputStream("sample_data.csv"), ",");
			cleanser.run(new FileOutputStream("output.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
