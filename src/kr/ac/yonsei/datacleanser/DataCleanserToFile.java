package kr.ac.yonsei.datacleanser;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class DataCleanserToFile {
	public static void main(String[] args) {
		try {
			// Open file stream reader
			Scanner scanner = new Scanner(new FileInputStream("sample_data.csv"), "UTF-8");

			PrintWriter out = new PrintWriter(new FileOutputStream("output.txt"));

			// Read each line
			while (scanner.hasNextLine()) {
				// Print line
				String line = scanner.nextLine();
				out.println(line);
			}

			out.close();
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
