package kr.ac.yonsei.datacleanser;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class DataCleanserToScreen {
	public static void main(String[] args) {
		try {
			// Open file stream reader
			Scanner scanner = new Scanner(new FileInputStream("sample_data.csv"), "UTF-8");

			// Read each line
			while (scanner.hasNextLine()) {
				// Print line
				String line = scanner.nextLine();
				System.out.println(line);
			}

			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
