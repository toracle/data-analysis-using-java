package kr.ac.yonsei.datacleanser;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class NodeIndexer {
	private InputStream is;
	private String delim;
	
	public NodeIndexer(InputStream is, String delim) {
		this.is = is;
		this.delim = delim; 
	}
	
	public Map<String, Integer> getNodeIndexMap() {
		Map<String, Integer> nodeIndexMap = new HashMap<String, Integer>();
		
		Scanner scanner = new Scanner(is);
		
		while(scanner.hasNextLine()) {
			String[] cols = scanner.nextLine().trim().split(delim);
			putNode(nodeIndexMap, cols[0]);
			putNode(nodeIndexMap, cols[1]);
		}
		
		scanner.close();
		
		return nodeIndexMap;
	}
	
	private void putNode(Map<String, Integer> map, String label) {
		if (map.containsKey(label) == false) {
			map.put(label, map.size()+1);
		}
	}
}
