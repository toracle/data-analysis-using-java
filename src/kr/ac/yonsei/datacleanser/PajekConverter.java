package kr.ac.yonsei.datacleanser;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Scanner;

public class PajekConverter {
	private InputStream is;
	private String delim;
	
	public PajekConverter(InputStream is, String delim) {
		this.is = is;
		this.delim = delim;
	}
	
	public void run(OutputStream os, Map<String, Integer> nodeIndexMap) {
		Scanner scanner = new Scanner(is, "UTF-8");
		PrintWriter writer = new PrintWriter(os);
		
		printVertices(nodeIndexMap, writer);
		printEdges(scanner, writer, nodeIndexMap);
		
		writer.flush();
		scanner.close();
	}

	private void printVertices(Map<String, Integer> nodeIndexMap, PrintWriter writer) {
		writer.println("*Vertices " + nodeIndexMap.size());
		
		for (Map.Entry<String, Integer> entry : nodeIndexMap.entrySet()) {
			writer.println(entry.getValue() + " " + entry.getKey());
		}
	}

	private void printEdges(Scanner scanner, PrintWriter writer, Map<String, Integer> nodeIndexMap) {
		writer.println("*Edges");

		// 한줄씩 읽어들인다.
		while(scanner.hasNextLine()) {
			// 컬럼을 나눈다.
			String[] cols = scanner.nextLine().trim().split(delim);
			
			// 라벨을 인덱스로 변환한다.
			int col1Index = nodeIndexMap.get(cols[0]);
			int col2Index = nodeIndexMap.get(cols[1]);
			
			// Edge를 출력한다.
			writer.println(col1Index + " " + col2Index + " " + cols[2]);
		}
	}
}
