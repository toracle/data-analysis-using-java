package kr.ac.yonsei.datacleanser;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Map;

public class PajekConverterRunner {

	public static void main(String[] args) {
		try {
			NodeIndexer indexer = new NodeIndexer(new FileInputStream("output.txt"), ",");
			Map<String, Integer> nodeIndexMap = indexer.getNodeIndexMap();
			
			PajekConverter converter = new PajekConverter(new FileInputStream("output.txt"), ",");
			converter.run(new FileOutputStream("output.net"), nodeIndexMap);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
