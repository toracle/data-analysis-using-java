package kr.ac.yonsei.homework;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Random;

public class ErdosRenyiRandomGenerator {
	public static void generatePajek(int n, double p, Writer writer) throws IOException {
		writer.write("*Vertices " + n + "\n");
		
		writer.write("*Edges\n");
		
		Random rand = new Random();
		
		// Pajek 데이터에서는 노드 번호가 1부터 시작해야 해서 1부터 세도록 함.
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				if (rand.nextDouble() <= p) {
					writer.write(i + " " + j + "\n");
				}
			}
		}
	}

	public static void main(String[] args) {
		try {
			// 파일 출력에 관련한 부분을 pajek 데이터 출력하는 부분(프로시저)과 분리하여서, 
			// 데이터 출력 부분에서는 파일 출력에 대해서 신경쓰지 않아도 되게 함.
			FileOutputStream fos = new FileOutputStream("output.net");
			OutputStreamWriter writer = new OutputStreamWriter(fos);
			
			// 실제 데이터를 쓰는 부분
			generatePajek(10, 0.3, writer);
			
			// 파일을 닫는다.
			// 파일은 꼭 닫아줘야 해요. 파일을 닫지 않으면 모든 데이터가 파일에 쓰여지지 않을 수 있어요.
			writer.close();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
