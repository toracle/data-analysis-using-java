package kr.ac.yonsei.homework;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Scanner;

public class WordGraphGenerator {
	public static void generate(InputStream is, Writer writer) throws IOException {
		// UTF-8 부분에는, 읽어들일 텍스트 파일의 인코딩을 지정합니다. 
		// 한글 문서를 처리할 때는 대부분 EUC-KR, UTF-8 둘 중 하나를 선택하면 처리 가능합니다.
		// 영문 문서일 때는 인코딩 인자를 제외해도 처리 가능합니다.
		Scanner scanner = new Scanner(is, "UTF-8");
		
		while(scanner.hasNextLine()) {
			// 파일에서 한 줄을 읽어들입니다.
			String line = scanner.nextLine();

			// 읽어들인 한 줄에 대해서, 공백을 기준으로 단어들을 잘라내어 배열에 저장합니다.
			String[] words = line.split(" ");
			
			for (int i = 0; i < words.length; i++) {
				// 이전에는 j=0부터 시작했는데, 왜 j=i부터 시작하냐면, 이 graph는 undirected graph이기 때문에 그렇습니다.
				// full matrix가 아니라 upper diagonal half matrix를 구성하게 됩니다.
				for (int j = i; j < words.length; j++) {
					// 만약 i와 j가 같으면, 즉 self loop가 되면
					if (i == j) {
						// 안쪽 for문에서 이후 남아있는 명령문을 건너뛰고 다음 j값으로 넘어갑니다.
						// 즉, 만약 i=0, j=0인 상태라면, write를 수행하지 않고 i=0, j=1로 넘어갑니다.
						continue;
					}
					
					// 문장부호를 제거한 단어를 다시 받아옵니다.
					String cleansedWord1 = getCleansed(words[i]); 
					String cleansedWord2 = getCleansed(words[j]); 
					
					// 최종 단어가 남아있지 않은 경우에는 건너뜁니다.
					if (cleansedWord1.length() == 0 || cleansedWord2.length() == 0) {
						continue;
					}

					// 파일에 기록합니다.
					writer.write(cleansedWord1 + "," + cleansedWord2 + "\n");
				}
			}
		}
		
		scanner.close();
	}
	
	public static String getCleansed(String s) {
		// 단어에 붙어있는 문장 부호들을 제거합니다.
		return s.replace(".", "").replace(",", "").replace("!", "").replace("\"", "");
	}
	
	public static void main(String[] args) {
		
		try {
			FileInputStream fis = new FileInputStream("kimspeech.txt");
			FileOutputStream fos = new FileOutputStream("kimspeech_out.csv");
			OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
			
			generate(fis, osw);
			
			osw.close();
			fos.close();
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
