package kr.ac.yonsei.homework;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;

public class WordGraphGeneratorAsPajek {
	/**
	 * 파일로부터 노드셋을 읽어들입니다.
	 * 
	 * @param is 읽어들일 파일 스트림
	 * @return 노드셋 Map (노드 label, 노드 일련번호)
	 */
	public static HashMap<String, Integer> getNodeset(InputStream is) {
		// 빈 Map을 생성합니다. Map은, [키, 값]으로 이루어진 공간입니다. 
		HashMap<String, Integer> nodeSet = new HashMap<String, Integer>();
		
		Scanner scanner = new Scanner(is, "UTF-8");
		
		// 다음 줄이 있을 동안 수행합니다.
		while(scanner.hasNextLine()) {
			// 한 줄을 읽어들입니다.
			String line = scanner.nextLine();
			
			// 공백으로 단어를 분리합니다.
			String[] words = line.split(" ");
			
			// 각 단어에 대해서
			for (String word : words) {
				// 단어에서 부호를 제거합니다.
				String cleansedWord = getCleansed(word);
				
				// 단어 길이가 0인 경우는 통과합니다.
				if (cleansedWord.length() == 0) {
					continue;
				}
				
				// 노드셋에 해당 단어가 등재되어 있지 않은 경우
				if (nodeSet.containsKey(cleansedWord) == false) {
					// 노드셋에 등록하면서, 일련번호를 붙입니다.
					// 일련번호는 현재 노드셋 크기+1으로 붙입니다. 
					// (첫번째 노드는 노드셋 크기가 0인 상태였을테니 +1 해서 1번이 됩니다. 두번째 노드는 2번, ...)
					nodeSet.put(cleansedWord, nodeSet.size()+1);
				}
			}
		}
		
		scanner.close();
		
		return nodeSet;
	}
	
	
	private static void printNodes(Writer writer, HashMap<String, Integer> nodeSet) throws IOException {
		writer.write("*Vertices " + nodeSet.size() + "\n");
		// 문법이 좀 복잡한데, [키, 값]의 집합으로 이루어진 Map의 모든 [키, 값]을 순회하기 위해서는 아래와 같이 합니다. (역시 idiom.)
		// entry라는 변수에, getKey()로 키를 받아올 수 있고, getValue()로 값을 받아올 수 있도록 담겨옵니다.
		for (Entry<String, Integer> entry : nodeSet.entrySet()) {
			writer.write(entry.getValue() + " " + entry.getKey() + "\n");
		}
	}


	private static void printEdges(InputStream is, Writer writer, HashMap<String, Integer> nodeSet) throws IOException {
		writer.write("*Edges\n");

		// UTF-8 부분에는, 읽어들일 텍스트 파일의 인코딩을 지정합니다. 
		// 한글 문서를 처리할 때는 대부분 EUC-KR, UTF-8 둘 중 하나를 선택하면 처리 가능합니다.
		// 영문 문서일 때는 인코딩 인자를 제외해도 처리 가능합니다.
		Scanner scanner = new Scanner(is, "UTF-8");

		while(scanner.hasNextLine()) {
			// 파일에서 한 줄을 읽어들입니다.
			String line = scanner.nextLine();

			// 읽어들인 한 줄에 대해서, 공백을 기준으로 단어들을 잘라내어 배열에 저장합니다.
			String[] words = line.split(" ");
			
			for (int i = 0; i < words.length; i++) {
				// 이전에는 j=0부터 시작했는데, 왜 j=i부터 시작하냐면, 이 graph는 undirected graph이기 때문에 그렇습니다.
				// full matrix가 아니라 upper diagonal half matrix를 구성하게 됩니다.
				for (int j = i; j < words.length; j++) {
					// 만약 i와 j가 같으면, 즉 self loop가 되면
					if (i == j) {
						// 안쪽 for문에서 이후 남아있는 명령문을 건너뛰고 다음 j값으로 넘어갑니다.
						// 즉, 만약 i=0, j=0인 상태라면, write를 수행하지 않고 i=0, j=1로 넘어갑니다.
						continue;
					}
					
					String cleansedWord1 = getCleansed(words[i]); 
					String cleansedWord2 = getCleansed(words[j]); 
					
					if (cleansedWord1.length() == 0 || cleansedWord2.length() == 0) {
						continue;
					}
					
					int word1Index = nodeSet.get(cleansedWord1);
					int word2Index = nodeSet.get(cleansedWord2);
					
					writer.write(word1Index + " " + word2Index + "\n");
				}
			}
		}
		
		scanner.close();
	}
	
	public static String getCleansed(String s) {
		// 단어에 붙어있는 문장 부호들을 제거합니다.
		return s.replace(".", "").replace(",", "").replace("!", "").replace("\"", "");
	}
	
	public static void generate(InputStream is, Writer writer, HashMap<String, Integer> nodeSet) throws IOException {
		// 이 feff를 파일에 기록하는건 좀 기술적인 이유 때문인데, 간단하게 설명하자면 Pajek에서 한글을 인식하도록 파일에 표시하는 것입니다. 
		writer.write('\ufeff');
		
		printNodes(writer, nodeSet);
		printEdges(is, writer, nodeSet);
	}

	public static void main(String[] args) {
		HashMap<String, Integer> nodeSet = new HashMap<>();

		// Pajek의 net 파일 구조를 보면, 가장 첫 부분에 Vertices의 갯수와 Vertices의 목록을 출력하게 되어있습니다.
		// 때문에 net 파일을 기록하기 위해서는 edge를 출력하기 이전에 모든 노드의 갯수와, 노드의 종류를 알고 있어야 합니다.
		// 이렇게 하기 위해서는 파일을 두번 읽어야 합니다. 첫번째는 노드 갯수를 알고 일련번호를 붙이기 위해서, 
		// 두번째는 edge를 기록하기 위해서. 
		try {
			FileInputStream fis = new FileInputStream("kimspeech.txt");
			// 파일로부터 노드셋을 읽어들입니다.
			nodeSet = getNodeset(fis);
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
			
		}
		
		try {
			FileInputStream fis = new FileInputStream("kimspeech.txt");
			FileOutputStream fos = new FileOutputStream("kimspeech_out.net");
			OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");

			// 파일을 기록합니다.
			generate(fis, osw, nodeSet);
			
			osw.close();
			fos.close();
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
