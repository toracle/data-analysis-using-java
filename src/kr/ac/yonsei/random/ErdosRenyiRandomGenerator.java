package kr.ac.yonsei.random;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Random;

public class ErdosRenyiRandomGenerator {
	public static void generate(int n, double p) {
		Random rand = new Random();
		
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (rand.nextDouble() <= p) {
					System.out.println(i + "," + j);
				}
			}
		}
	}
	
	public static void generateAsPajek(int n, double p, Writer writer) throws IOException {
		writer.write("*Vertices " + n + "\n");
		
		writer.write("*Edges\n");
		Random rand = new Random();
		
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				if (rand.nextDouble() <= p) {
					writer.write(i + " " + j + "\n");
				}
			}
		}
	}

	public static void main(String[] args) {
		generate(10, 0.3);
		
		try {
			FileOutputStream fos = new FileOutputStream("output.net");
			OutputStreamWriter writer = new OutputStreamWriter(fos);
			generateAsPajek(10, 0.3, writer);
			writer.close();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
